-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis, demawi
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: GetJournalQuestConditionItemLink.lua
-- File Description: This class is only used for GetJournalQuestConditionItemLink
-- Load Order Requirements: none
--
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory
local utils = RbI.class.Utilities


------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- DailyWrits conditionType==44 ----------------------------------------------
------------------------------------------------------------------------------

-- craftType -> Craftable ItemIds
local scalingItemIds = {
    [1]= {43529, 43530, 43531, 43532, 43533, 43534, 43535, 43537, 43538, 43539, 43562, 43540, 43541, 43542}, -- blacksmithing
    [2]= {43543, 44241, 43544, 43545, 43564, 43546, 43547, 43548, 43550, 43551, 43552, 43563, 43553, 43554, 43555}, -- clothier
    [6]= {43549, 43556, 43557, 43558, 43559, 43560}, -- woodworking
    [7]= {43536, 43561}, -- jewelry
}
local nonScalingItemIds = {}
local function addToMapList(mapList, craftType, id)
    local ids = mapList[craftType]
    if(ids == nil) then
        ids = {}
        mapList[craftType] = ids
    end
    ids[#ids+1] = id
end
local function addScalingItemId(craftType, id)
    addToMapList(scalingItemIds, craftType, id)
end
local function addNonScalingItemId(craftType, id)
    addToMapList(nonScalingItemIds, craftType, id)
end

local function createItemLink(itemId, quality, lvl)
    return string.format("|H1:item:%d:%d:%d:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", itemId, quality or 0, lvl or 0)
end
-- thx@LibLazyCrafting for this method
local function getCP_and_Quality(level, cp, quality)
    if(cp == 0) then
        if(level < 5) then
            if(level == 1) then
                return 29 + quality
            elseif(level == 4) then
                return 24 + quality
            end
        else
            return 19 + quality
        end
    else
        if(cp < 101) then
            return 115 + quality * 10 + cp/10 - 1
        end
        if(cp < 151) then
            return 217 + 18 * (cp-100)/10 + quality
        elseif(cp == 160) then
            return 365 + quality
        end
    end
end
local function createItemLinkWithLvlCpQuality(itemId, lvl, cp, quality)
    return createItemLink(itemId, getCP_and_Quality(lvl, cp, quality or 1), lvl)
end

local itemFinderProcessor = {
    [ITEMTYPE_WEAPON]= function(link, id)
        local weaponType = GetItemLinkWeaponType(link)
        local set = GetItemLinkSetInfo(link)
        local bindType = GetItemLinkBindType(link)
        local trait = GetItemLinkTraitType(link)
        local style = GetItemLinkItemStyle(link)
        local value = GetItemLinkValue(link)
        if(not set and bindType == -1 and trait == 0 and style == 0 and value > 0 and weaponType == WEAPONTYPE_SHIELD) then
            -- d("BaseWeapon: "..link)
        end
    end,
    [ITEMTYPE_ARMOR]= function(link, id)
        local set = GetItemLinkSetInfo(link)
        local bindType = GetItemLinkBindType(link)
        local trait = GetItemLinkTraitType(link)
        local style = GetItemLinkItemStyle(link)
        local value = GetItemLinkValue(link)
        if(not set and bindType == -1 and trait == 0 and style == 0 and value > 0) then
            --d("BaseArmor: "..link)
        end
    end,
    [ITEMTYPE_POTION] = function(link, id)
        addScalingItemId(CRAFTING_TYPE_ALCHEMY, id)
    end,
    [ITEMTYPE_POISON] = function(link, id)
        addScalingItemId(CRAFTING_TYPE_ALCHEMY, id)
    end,
    [ITEMTYPE_REAGENT] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_ALCHEMY, id)
    end,
    [ITEMTYPE_POTION_BASE] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_ALCHEMY, id)
    end,
    [ITEMTYPE_POISON_BASE] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_ALCHEMY, id)
    end,
    [ITEMTYPE_FOOD] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_PROVISIONING, id)
    end,
    [ITEMTYPE_DRINK] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_PROVISIONING, id)
    end,
    [ITEMTYPE_ENCHANTING_RUNE_ESSENCE] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_ENCHANTING, id)
    end,
    [ITEMTYPE_ENCHANTING_RUNE_POTENCY] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_ENCHANTING, id)
    end,
    [ITEMTYPE_ENCHANTING_RUNE_ASPECT] = function(link, id)
        addNonScalingItemId(CRAFTING_TYPE_ENCHANTING, id)
    end,
    [ITEMTYPE_GLYPH_ARMOR] = function(link, id)
        addScalingItemId(CRAFTING_TYPE_ENCHANTING, id)
    end,
    [ITEMTYPE_GLYPH_WEAPON] = function(link, id)
        addScalingItemId(CRAFTING_TYPE_ENCHANTING, id)
    end,
    [ITEMTYPE_GLYPH_JEWELRY] = function(link, id)
        addScalingItemId(CRAFTING_TYPE_ENCHANTING, id)
    end,
}

local alreadySearched = false
local function itemIdsLoaded()
    if(alreadySearched) then return end
    alreadySearched = true
    local itemFinderI = 0
    local itemFinderLastFound = 0
    local itemFinderCount = 0
    local itemFinderStepSize = 10000
    local function itemIdLoader()
        local link
        for i=itemFinderI, itemFinderI+(itemFinderStepSize-1) do
            link = createItemLink(i, 0, 0)
            local type = GetItemLinkItemType(link)
            if(type ~= 0) then -- and GetItemLinkName(link) ~=""
                itemFinderLastFound = i
                itemFinderCount = itemFinderCount + 1
                local processor = itemFinderProcessor[type]
                if(processor) then processor(link, i) end
            end
        end
        itemFinderI = itemFinderI + itemFinderStepSize
        -- d("ItemFounder: Findings: "..itemFinderCount.." LastFound: "..itemFinderLastFound.." "..link.." "..itemFinderI)
        if(itemFinderLastFound+10000 > itemFinderI) then itemIdLoader() end
    end
    local start = GetGameTimeMilliseconds()
    itemIdLoader()
    d("itemIdLoader: "..tostring((GetGameTimeMilliseconds()-start)/1000).."secs")
end
itemIdsLoaded()

local cp = function(cp) return cp+50 end
local scalingLevelsSmithing = {1, 16, 26, 36, 46, cp(10), cp(40), cp(70), cp(90), cp(150) }
local scalingLevels = {
    [CRAFTING_TYPE_BLACKSMITHING] = scalingLevelsSmithing,
    [CRAFTING_TYPE_CLOTHIER] = scalingLevelsSmithing,
    [CRAFTING_TYPE_WOODWORKING] = scalingLevelsSmithing,
    [CRAFTING_TYPE_JEWELRYCRAFTING] = scalingLevelsSmithing,
    [CRAFTING_TYPE_ALCHEMY] = {1, 3, 20, 30, cp(150) },
    [CRAFTING_TYPE_ENCHANTING] = { 1, 5, 10, 15, 20, 25, 30, 35, 40, cp(10), cp(30), cp(50), cp(70), cp(100), cp(150), cp(160) },
}
local function GetItemLinkSearch(optCraftType, predFn, ...)
    local craftTypeScalingItemIdMap
    local craftTypeNonScalingItemIdMap
    if(optCraftType) then
        craftTypeScalingItemIdMap = { [optCraftType] = scalingItemIds[optCraftType] }
        craftTypeNonScalingItemIdMap = { [optCraftType] = nonScalingItemIds[optCraftType] }
    else
        craftTypeScalingItemIdMap = scalingItemIds
        craftTypeNonScalingItemIdMap = nonScalingItemIds
    end
    for craftType, ids in pairs(craftTypeScalingItemIdMap) do
        for _, id in pairs(ids) do
            for _, lvl in pairs(scalingLevels[craftType]) do
                local cp = 0
                if(lvl > 50) then
                    cp = lvl - 50
                    lvl = 50
                end
                local link = createItemLinkWithLvlCpQuality(id, lvl, cp)
                if predFn(link, ...) then return link end
            end
        end
    end
    for craftType, ids in pairs(craftTypeNonScalingItemIdMap) do
        for _, id in pairs(ids) do
            local link = createItemLink(id, 0, 0)
            if predFn(link,...) then return link end
        end
    end
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- MasterWrits conditionType==48 ---------------------------------------------
------------------------------------------------------------------------------

local linkGenerator = {

}

local function getParseResult(questIdx, stepIdx, conditionIdx)
    local questName = GetJournalQuestInfo(questIdx)
    local crafting_type_list = LibCraftText.MasterQuestNameToCraftingTypeList(questName)
    if not crafting_type_list then return nil end
    for _,crafting_type in pairs(crafting_type_list) do
        local conditionText = GetJournalQuestConditionInfo(questIdx, stepIdx, conditionIdx)
        if conditionText ~= "" then
            local parse_result = LibCraftText.ParseMasterCondition(crafting_type, conditionText)
            if parse_result then
                return crafting_type, parse_result
            end
        end
    end
end

local function GetMasterWritItemLink(optCraftType, questIdx, stepIdx, conditionIdx)
    local conditionText, current, max, isFailCondition, isComplete, isCreditShared, isVisible, conditionType = GetJournalQuestConditionInfo(questIdx, stepIdx, conditionIdx)
    if(current == max) then return end
    d(conditionText.." "..current.." "..max.." "..conditionType)
    local crafting_type, parseResult = getParseResult(questIdx, stepIdx, conditionIdx)
    --local parseResult = LibCraftText.ParseQuest(questIdx)
    d("Masterwrit")
    if(true) then
        for key, value in pairs(parseResult) do
            if(type(value)=="table") then
                -- d(""..key..": "..tostring(table.concat(value, ", ")))
                d(""..key..": "..tostring(value[1]))
            else
                d(""..key..": "..tostring(value))
            end
        end
    else
        for i, result in pairs(parseResult) do
            d("RESULT "..i)
            -- quest_index, step_index, condition_index
            -- .item: tbl
            -- .set: tbl
            -- .material: tbl
            -- .motif: tbl
            -- .quality: tbl
            -- .trait: tbl
            for key, value in pairs(result) do
                if(type(value)=="table") then
                    -- d(""..key..": "..tostring(table.concat(value, ", ")))
                    d(""..key..": "..tostring(value[1]))
                else
                    d(""..key..": "..tostring(value))
                end

            end
        end
    end

end

local function getQuestConditionIdx(questIdx)
    for stepIdx=1, GetJournalQuestNumSteps(questIdx) do
        for condIdx=1, GetJournalQuestNumConditions(questIdx, stepIdx) do
            local _, _, max, _, _, _, isVisible, conditionType = GetJournalQuestConditionInfo(questIdx, stepIdx, condIdx)
            if(isVisible and max > 0 and (conditionType == 44 or conditionType == 48)) then
                return stepIdx, condIdx
            end
        end
    end
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Public functions ----------------------------------------------------------
------------------------------------------------------------------------------

local function GetJournalQuestConditionItemLink(questIdx, stepIdx, conditionIdx, optCraftType)
    if(conditionIdx == nil) then stepIdx, conditionIdx = getQuestConditionIdx(questIdx) end
    local checks = 0
    local conditionType = select(8, GetJournalQuestConditionInfo(questIdx, stepIdx, conditionIdx))
    if(conditionType == 44) then
        return GetItemLinkSearch(optCraftType, function(...) checks = checks + 1 return DoesItemLinkFulfillJournalQuestCondition(...) end, questIdx, stepIdx, conditionIdx), checks
    elseif(conditionType == 48) then
        return GetMasterWritItemLink(optCraftType, questIdx, stepIdx, conditionIdx)
    end
end


------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Testing/Debugging/Retrieval functions -------------------------------------
------------------------------------------------------------------------------

-- Used to retrieve the smithing base item ids.
-- only works when visiting a smithing crafting station
local function getAllSmithingWritItemIds()
    local result = {}
    local lastId
    for j =1, GetNumSmithingPatterns() do
        local _,_,n = GetSmithingPatternMaterialItemInfo(j, 1)
        local curLink = GetSmithingPatternResultLink(j, 1, n, 1, 1, 1)
        local id = GetItemLinkItemId(curLink)
        if(lastId == nil or lastId ~= id) then
            result[#result+1] = id
            lastId = id
        end
    end
    local craftType = GetCraftingInteractionType()
    RbI.PrintToClipBoard("["..craftType.."]= {"..table.concat(result, ", ").."},")
    return result
end

local function dumpQuestEntry(questIdx)
    local questName = GetJournalQuestName(questIdx)
    if (GetJournalQuestType(questIdx) == QUEST_TYPE_CRAFTING and GetJournalQuestRepeatType(questIdx)==QUEST_REPEAT_DAILY) then
        d("QuestName: "..questName)
        for stepIdx=1, GetJournalQuestNumSteps(questIdx) do
            for condIdx=1, GetJournalQuestNumConditions(questIdx, stepIdx) do
                local conditionText, current, max, isFailCondition, isComplete, isCreditShared, isVisible, conditionType = GetJournalQuestConditionInfo(questIdx, stepIdx, condIdx)
                if(isVisible and max > 0 and (conditionType == 44 or conditionType == 48)) then
                    local link = GetJournalQuestConditionItemLink(questIdx, stepIdx, condIdx)
                    d(tostring(conditionText).." => "..tostring(link))
                end
            end
        end
    end
end

EVENT_MANAGER:RegisterForEvent("dumpQuestEntry", EVENT_QUEST_ADDED, function(event, questIdx, name) dumpQuestEntry(questIdx) end)

-- masterwrits-factors: traits(9), style(?), set(?), quality(2)
RbI.debug = RbI.debug or {}
-- RbI.debug.dumpWrits()
RbI.debug = {
    dumpWrits = function()
        local fullChecks = 0
        local result, checks = GetItemLinkSearch(nil, function()
            fullChecks = fullChecks + 1
            return false
        end)
        d("max # of itemlinks to check: "..fullChecks)
        local start = GetGameTimeMilliseconds()
        local checks = 0
        local maxChecks = 0
        for questIdx=1, MAX_JOURNAL_QUESTS do -- not GetNumJournalQuests()!!! the quests aren't saved sequentially
            dumpQuestEntry(questIdx)
        end
        d("dumpWritsTime: "..tostring((GetGameTimeMilliseconds()-start)/1000).."secs with "..tostring(checks).." checks MaxChecks:"..maxChecks)
    end

}